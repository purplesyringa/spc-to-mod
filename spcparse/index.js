const util = require("./util");
const SPCParser = require("./spcparser");
const INSParser = require("./insparser");

module.exports = {
	util,
	SPCParser,
	INSParser
};