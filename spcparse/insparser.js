const util = require("./util");
const fs = require("fs");

class INSParser {
	constructor(file) {
		this.file = file;
		this.fd = fs.openSync(this.file, "r");
	}

	load(offset, size) {
		let buffer = Buffer.alloc(size);
		fs.readSync(this.fd, buffer, 0, size, offset);
		/*
		for(let i = 0; i < buffer.length; i += 2) {
			let tmp = buffer[i];
			buffer[i] = buffer[i + 1];
			buffer[i + 1] = tmp;
		}
		*/
		for(let i = 0; i < buffer.length; i++) {
			buffer[i] += 0x80;
		}
		return buffer;
	}
};

module.exports = INSParser;