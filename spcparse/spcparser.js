const util = require("./util");

class SPCParser {
	constructor(stream, patternOffset, instrumentOffset) {
		this.stream = stream;
		this.pos = 0;
		this.patternOffset = patternOffset || 0o40000;
		this.instrumentOffset = instrumentOffset || 0o4000;
		this.sstVersion = "SST";
		this.parse();
	}

	read(bytes) {
		this.pos += bytes;
		return this.stream.read(bytes);
	}
	tell() {
		return this.pos;
	}
	seek(pos) {
		if(pos < this.pos) {
			throw new Error("Seeking back");
		}

		this.read(pos - this.pos);
	}

	parse() {
		this.tempo = util.from16(this.read(2)) + 1;

		this.patternSeq = [];
		do {
			let address = util.from16(this.read(2));
			if(address != 0) {
				this.patternSeq.push(address - this.patternOffset);
			} else {
				break;
			}
		} while(true);

		this.patternList = Array.from(this.patternSeq)
			.filter((val, i, arr) => arr.indexOf(val) == i)
			.sort((a, b) => a - b);

		this.patternSeq = this.patternSeq.map(patternOffset => {
			return this.patternList.indexOf(patternOffset);
		});

		this.title = this.read(31).toString().replace(/[^\x20-\x7E]/g, "").trim();
		util.assert.equal(this.read(1)[0], 0, "Expected NUL byte after title");

		this.instruments = [];
		let instrumentString = this.read(this.patternList[0] - this.tell());
		for(let i = 0; i < instrumentString.length && this.instruments.length < 8; i += 8) {
			let name = instrumentString.slice(i, i + 8).toString().replace(/\0/g, "").trimRight();
			if(name) {
				this.instruments.push(name);
			} else {
				break;
			}
		}

		// Parse patterns
		this.patterns = [];
		this.endOfInstrument = {};
		this.patternList.forEach((offset, patternId) => {
			this.parsePattern(offset, patternId);
		});

		this.findIntersections();

		this.patterns.forEach(pattern => {
			pattern.forEach(row => {
				return row.forEach(note => {
					if(note.baseInstrument == -1) {
						note.instrumentId = 0;
						return;
					}

					let id = Object.keys(this.instrumentList).find(instrument => {
						return this.instrumentList[instrument].instrument == note.baseInstrument;
					});
					note.instrumentId = parseInt(id);
				});
			});
		});

		// Recalculate instrument ends
		let oldEndOfInstrument = this.endOfInstrument;
		this.endOfInstrument = {};

		let sortedInstrumentList = Object.keys(this.instrumentList)
			.map(id => this.instrumentList[id])
			.sort((a, b) => {
				return a.instrument - b.instrument;
			});

		sortedInstrumentList.forEach((instrument, i) => {
			let start = instrument.instrument;
			let end = oldEndOfInstrument[start];

			let next = sortedInstrumentList[i + 1];
			if(next && next.instrument > end) {
				end = next.instrument;
			}

			this.endOfInstrument[start] = end;
		});

		if(this.sstVersion == "SST1") {
			// Metadata after last pattern
			this.title = this.read(32).toString().replace(/[^\x20-\x7E]/g, "").trim();

			this.instruments = [];
			for(let i = 0; i < 8; i++) {
				let name = this.read(8).toString().replace(/\0/g, "").trimRight();
				if(name) {
					this.instruments.push(name);
				} else {
					break;
				}
			}
		}
	}
	parsePattern(offset, patternId) {
		// Seek to offset
		this.seek(offset);

		let pattern = [];
		let rowId = 0;
		while(true) {
			let instrument1 = this.read(2);
			if(instrument1 === null) {
				break;
			}
			instrument1 = util.from16(instrument1);

			let frequency1 = util.from16(this.read(2));
			let instrument2 = util.from16(this.read(2));
			let frequency2 = util.from16(this.read(2));
			let instrument3 = util.from16(this.read(2));
			let command3 = util.from16(this.read(2));

			if(instrument1 == 0 && frequency1 == 0 && instrument2 == 0 && frequency2 == 0 && instrument3 == 0 && command3 == 0) {
				// SST
				this.patterns.push(pattern);
				return;
			} else if(instrument1 == 0 && frequency1 == 0 && instrument2 == 0x5353 && frequency2 == 0x3154 && instrument3 == 0 && command3 == 0) {
				// SST1
				this.patterns.push(pattern);
				this.sstVersion = "SST1";
				return;
			} else {
				let newRow = [
					{
						instrument: instrument1 - this.instrumentOffset,
						frequency: frequency1
					},
					{
						instrument: instrument2 - this.instrumentOffset,
						frequency: frequency2
					},
					{
						instrument: instrument3 - this.instrumentOffset,
						command: command3
					}
				];
				let oldRow = pattern.slice(-1)[0];

				pattern.push(this.parseRow(oldRow, newRow, patternId, rowId));
			}
			rowId++;
		}
	}
	parseRow(oldRow, newRow, patternId, rowId) {
		let resRow = [null, null, null];

		for(let i = 0; i < 2; i++) {
			let expectedInstrument = oldRow && (oldRow[i].instrument + Math.floor(newRow[i].frequency / 0xFFFF * this.tempo));

			if(newRow[i].frequency == 0) {
				if(oldRow && oldRow[i].frequency != 0) {
					// Note stop
					resRow[i] = {
						instrument: -1,
						baseInstrument: -1,
						frequency: 0,
						note: "",
						newNote: false,
						noteStop: true,
						hasNote: false
					};
				} else {
					// Note stop continue
					resRow[i] = {
						instrument: -1,
						baseInstrument: -1,
						frequency: 0,
						note: "",
						newNote: false,
						noteStop: false,
						hasNote: false
					};
				}
			} else if(oldRow && Math.abs(expectedInstrument - newRow[i].instrument) <= 1) {
				// Same note
				resRow[i] = {
					instrument: newRow[i].instrument,
					baseInstrument: oldRow[i].baseInstrument,
					frequency: newRow[i].frequency,
					note: this.findNote(newRow[i].frequency, patternId, rowId, i),
					newNote: false,
					noteStop: false,
					hasNote: true
				};
			} else {
				// New note
				resRow[i] = {
					instrument: newRow[i].instrument,
					baseInstrument: newRow[i].instrument,
					frequency: newRow[i].frequency,
					note: this.findNote(newRow[i].frequency, patternId, rowId, i),
					newNote: true,
					noteStop: false,
					hasNote: true
				};
			}
		}

		if(newRow[2].command == util.COMMAND_STOP) {
			if(oldRow && oldRow[2].command == util.COMMAND_PLAY) {
				// Note stop
				resRow[2] = {
					instrument: -1,
					baseInstrument: -1,
					frequency: 0,
					command: newRow[2].command,
					note: "",
					newNote: false,
					noteStop: true,
					hasNote: false
				};
			} else {
				// Note stop continue
				resRow[2] = {
					instrument: -1,
					baseInstrument: -1,
					frequency: 0,
					command: newRow[2].command,
					note: "",
					newNote: false,
					noteStop: false,
					hasNote: false
				};
			}
		} else if(oldRow && Math.abs(oldRow[2].instrument + this.tempo - newRow[2].instrument) <= 1) {
			// Same note
			resRow[2] = {
				instrument: newRow[2].instrument,
				baseInstrument: oldRow[2].baseInstrument,
				command: newRow[2].command,
				note: this.findNote(0xFFFF, patternId, rowId, 2),
				frequency: 0xFFFF,
				newNote: false,
				noteStop: false,
				hasNote: true
			};
		} else {
			// New note
			resRow[2] = {
				instrument: newRow[2].instrument,
				baseInstrument: newRow[2].instrument,
				command: newRow[2].command,
				note: this.findNote(0xFFFF, patternId, rowId, 2),
				frequency: 0xFFFF,
				newNote: true,
				noteStop: false,
				hasNote: true
			};
		}

		for(let i = 0; i < 3; i++) {
			if(resRow[i].baseInstrument == -1) {
				continue;
			}

			let prevOld = this.endOfInstrument[resRow[i].baseInstrument];
			let tempo = Math.floor(resRow[i].frequency / 0xFFFF * this.tempo);
			this.endOfInstrument[resRow[i].baseInstrument] = Math.max(prevOld || 0, resRow[i].instrument + tempo);
		}

		return resRow;
	}

	findIntersections() {
		this.instrumentList = {};

		Object.keys(this.endOfInstrument)
			.sort((a, b) => a - b)
			.forEach((start, instrumentId) => {
				start = parseInt(start);
				let end = this.endOfInstrument[start];

				this.instrumentList[instrumentId] = {
					instrument: start
				};
			});

		if(this.sstVersion == "SST1") {
			// For SST1 which supports offset, check if they were used.
			this.checkSst1Offsets();
		}
	}
	checkSst1Offsets() {
		let sortedInstrumentList = Object.keys(this.instrumentList)
			.map(id => this.instrumentList[id])
			.sort((a, b) => {
				return a.instrument - b.instrument;
			});

		sortedInstrumentList.forEach((instrument, i) => {
			let before = sortedInstrumentList.slice(0, i)
				.find(instrument2 => {
					let dist = instrument.instrument - instrument2.instrument;
					return dist % 129 == 0;
				});

			if(before) {
				let dist = instrument.instrument - before.instrument;

				let beforeId = parseInt(Object.keys(this.instrumentList).find(key => this.instrumentList[key] == before));
				let id = parseInt(Object.keys(this.instrumentList).find(key => this.instrumentList[key] == instrument));

				if(this.instrumentList[beforeId].sst1Offset) {
					this.instrumentList[id].sst1Offset = this.instrumentList[beforeId].sst1Offset + dist;
					this.instrumentList[id].sst1Parent = this.instrumentList[beforeId].sst1Parent;
				} else {
					this.instrumentList[id].sst1Offset = dist;
					this.instrumentList[id].sst1Parent = beforeId;
				}

				let parentId = this.instrumentList[id].sst1Parent;
				let parentStart = this.instrumentList[parentId].instrument;
				let start = this.instrumentList[id].instrument;
				this.endOfInstrument[parentStart] = Math.max(this.endOfInstrument[parentStart], this.endOfInstrument[start]);
			}
		});
	}

	findNote(frequency, patternId, rowId, channelId) {
		let index = util.NOTES.findIndex(note => note[0] == frequency);
		if(index == -1) {
			let bestFit = Array.from(util.NOTES).sort((a, b) => Math.abs(a[0] - frequency) - Math.abs(b[0] - frequency))[0];
			if(Math.abs(bestFit[0] - frequency) <= util.MAX_NOTE_ERROR) {
				console.error("Mistaken frequency " + frequency + " at pattern " + patternId + ", row " + rowId + ", channel " + channelId + ". Best fit " + bestFit[1] + " was used.");
				return bestFit[1];
			}

			console.error("Unknown frequency " + frequency + " at pattern " + patternId + ", row " + rowId + ", channel " + channelId + ". It was marked as C-6 in MOD.");
			return "C-4";
		}
		return util.NOTES[index + util.NOTE_OFFSET][1];
	}
};

module.exports = SPCParser;