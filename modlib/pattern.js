const DynamicBuffer = require("DynamicBuffer");
const util = require("./util");

class Pattern {
	constructor(info) {
		this.rows = info.rows;
	}

	encode() {
		let pattern = new DynamicBuffer();
		this.rows.forEach(row => {
			pattern.concat(this.encodeRow(row));
		});
		for(let i = this.rows.length; i < 64; i++) {
			pattern.concat(this.encodeRow([]));
		}
		return pattern;
	}
	encodeRow(row) {
		let rawData = new DynamicBuffer();
		rawData.concat(this.encodeNote(row[0]));
		rawData.concat(this.encodeNote(row[1]));
		rawData.concat(this.encodeNote(row[2]));
		rawData.concat(this.encodeNote(row[3]));
		return rawData;
	}
	encodeNote(note) {
		if(!note || note.empty) {
			note = {
				period: 0,
				instrument: 0,
				effect: note && note.effect || null
			};
		} else {
			let sign = note.sign.substr(0, 2).replace("-", "");
			let octave = parseInt(note.sign[2]);
			note.period = util.NOTES[sign][octave];
		}

		let effect = this.encodeEffect(note.effect);

		let rawData = new Buffer(4);
		rawData.writeUInt8((note.period >> 8) | (note.instrument >> 4 << 4), 0);
		rawData.writeUInt8(note.period & 0xFF, 1);
		rawData.writeUInt8((note.instrument & 0xF) << 4 | (effect >> 8), 2);
		rawData.writeUInt8(effect & 0xFF, 3);
		return rawData;
	}
	encodeEffect(effect) {
		if(effect == null) {
			return 0;
		} else if(effect == "patternBreak") {
			return 0xD << 8;
		} else if(effect.indexOf("volume") == 0) {
			return (0xC << 8) | parseInt(effect.substr(6));
		} else if(effect.indexOf("sampleOffset") == 0) {
			return (0x9 << 8) | Math.round(parseInt(effect.substr(12)) / 512);
		} else if(effect.indexOf("setSpeed") == 0) {
			return (0xF << 8) | parseInt(effect.substr(8));
		} else {
			return 0;
		}
	}
};

module.exports = Pattern;