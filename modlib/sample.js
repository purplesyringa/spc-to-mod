const DynamicBuffer = require("DynamicBuffer");
const util = require("./util");

class Sample {
	constructor(info) {
		this.title = info.title;
		this.size = info.size;
		this.buffer = info.buffer;
		this.finetune = info.finetune;
	}

	encode() {
		let song = new DynamicBuffer();
		song.append(util.padNull(this.title, 22));
		song.concat(util.to16(this.size >> 1));
		song.write(this.encodeFinetune());
		song.write(util.SAMPLE_VOLUME);
		song.concat(new Buffer([0, 0]));
		song.concat(new Buffer([0, 1]));
		return song;
	}
	encodeFinetune() {
		return util.FINETUNE_TABLE.indexOf(Math.round(this.finetune / 16));
	}

	encodeData() {
		return this.buffer;
	}
};

module.exports = Sample;